export class Tarefa {

  constructor(
    public numero?: number,
    public id?: number,
    public nome?: string,
    public concluida?: boolean) {}
}
